--EXEC FIRST
CREATE DATABASE Mundial;

--EXEC SECOND
USE [Mundial];

CREATE  TABLE Perfiles
(
	IdPerfil	INT		IDENTITY(1,1) PRIMARY KEY ,
	Descripcion CHAR(15) NOT NULL
);

CREATE  TABLE Usuarios 
(
	IdUsuario	INT			IDENTITY(1,1) PRIMARY KEY,
	Usuario		CHAR(20)	UNIQUE,
	Contraseņa	CHAR(20)	NOT NULL,
	Nombre		CHAR(30)	NOT NULL,
	IdPerfil	INT			FOREIGN KEY REFERENCES Perfiles(IdPerfil)
);

CREATE  TABLE Calendario
(
	Id					INT			IDENTITY(1,1) PRIMARY KEY ,
	EquipoLocal			NCHAR(20)	NOT NULL,
	EquipoVisitante		NCHAR(20)	NOT NULL,
	ResultadoLocal		INT,
	ResultadoVisitante	INT,
	Fecha				DATETIME	NOT NULL,
	ImgLocal			NVARCHAR(100),
	ImgVisitante		NVARCHAR(100),
	Grupo				NVARCHAR(10)
);

CREATE  TABLE Resultados 
(
	IdResultado			INT			IDENTITY(1,1) PRIMARY KEY,
	idCalendario		INT			FOREIGN KEY REFERENCES Calendario(Id),
	ResultadoLocal		INT,
	ResultadoVisitante	INT,
	IdUsuario			INT			FOREIGN KEY REFERENCES Usuarios(IdUsuario)
);