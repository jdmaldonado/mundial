﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mundial.Models;
using Mundial.DataBase.Queries;

namespace Mundial.Services
{
    public class TeamsService
    {
        public static IEnumerable<Team> GetTeams()
        {
            
            IEnumerable<Team> teams = GetTeamsQuery.ExecuteGiven<Team>().ToList();
            var validateTeams = new List<Team>();
            foreach (var t in teams)
            {
                DateTime limit = t.fecha.AddHours(-1);
                if (limit <= DateTime.Now)
                {
                    t.status = false;
                }
                else
                {
                    t.status = true;
                }
                validateTeams.Add(t);
            }

            return validateTeams;
        }
    }
}