﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mundial.Models;
using Mundial.DataBase.Inserts;

namespace Mundial.Services
{
    public class CreateUserService 
    {
        public static int GetStatus(string nombre, string usuario, string contraseña, int idPerfil)
        {
            
            var status = CreateUserInsert.ExecuteInsert(nombre, usuario, contraseña, idPerfil);


            return status;
        }
    }
}