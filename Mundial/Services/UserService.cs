﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mundial.Models;
using Mundial.DataBase.Queries;

namespace Mundial.Services
{
    public class UserService
    {
        public static UserInfo GetUserInfo(string username)
        {

            UserInfo userInfo = GetUserInfoQuery.ExecuteGiven(username);

            userInfo.contraseña = null;

            return userInfo;
        }
    }
}