﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mundial.Models;
using Mundial.DataBase.Queries;

namespace Mundial.Services
{
    public class ProfileService
    {
        public static IEnumerable<Profile> GetProfiles()
        {

            IEnumerable<Profile> profiles = GetProfilesQuery.ExecuteGiven<Profile>().ToList();

            return profiles;
        }
    }
}