﻿using Mundial.DataBase.Queries;
using Mundial.Models;
using Nancy.Authentication.Basic;
using Nancy.Security;
using Simple.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mundial.Services
{
    public class UserValidator : IUserValidator
    {
        public IUserIdentity Validate(string username, string password)
        {
            var user = GetUserByCredentialsQuery.ExecuteGiven(username, password);
            if (user != null)
                return new UserIdentity { UserName = username };

            return null;
        }
    }
}