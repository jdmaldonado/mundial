﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mundial.Models;
using Mundial.DataBase.Inserts;
using Mundial.DataBase.Queries;

namespace Mundial.Services
{
    public class InsertScoreService 
    {
        public static dynamic GetStatus(string idCalendario, string local, string visitante, string Usuario)
        {
            int tipo;
            int id;
            var user = UserService.GetUserInfo(Usuario);
            var idResultado = GetScoreStatus.ExecuteGiven(idCalendario, user.idUsuario);
            if (idResultado != null)
            {
                tipo = 2;
                id = idResultado.idResultado;
            }
            else
            {
                tipo = 1;
                id = 0;
            }
            var status = InsertScoreInsert.ExecuteInsert(idCalendario, local, visitante, user.idUsuario, tipo, id);
                    


            return status;
        }
    }
}