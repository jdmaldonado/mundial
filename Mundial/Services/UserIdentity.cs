﻿using System;
using System.Collections.Generic;
using Nancy.Authentication.Basic;
using Nancy.Security;
using Simple.Data;
using System.Linq;
using System.Web;

namespace Mundial.Services
{
    public class UserIdentity : IUserIdentity
    {
        public string UserName { get; set; }
        public IEnumerable<string> Claims { get; set; }
    }
}