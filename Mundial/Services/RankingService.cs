﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mundial.Models;
using Mundial.DataBase.Queries;

namespace Mundial.Services
{
    public class RankingService
    {
        public static IEnumerable<Ranking> GetRanking()
        {

            IEnumerable<Ranking> ranking = GetRankingQuery.ExecuteGiven<Ranking>().ToList();


            return ranking;
        }
    }
}