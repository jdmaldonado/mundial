﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mundial.Models
{
    public class UserInfo
    {
        public string idUsuario             { get; set; }
        public string usuario               { get; set; }
        public string nombre                { get; set; }
        public string contraseña            { get; set; }
        public int idPerfil                 { get; set; }
    }
}