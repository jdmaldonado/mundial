﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mundial.Models
{
    public class Ranking
    {
        public string idUsuario             { get; set; }
        public string nombre                { get; set; }
        public int puntaje                 { get; set; }
    }
}