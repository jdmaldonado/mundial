﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mundial.Models
{
    public class Team
    {
        public string id                    { get; set; }
        public string equipoLocal           { get; set; }
        public string equipoVisitante       { get; set; }
        public string resultadoLocal        { get; set; }
        public string resultadoVisitante    { get; set; }
        public DateTime fecha               { get; set; }
        public string imgLocal              { get; set; }
        public string imgVisitante          { get; set; }
        public string grupo                 { get; set; }
        public Boolean status               { get; set; }
    }
}