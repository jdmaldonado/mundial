﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mundial.Models
{
    public class Score
    {
        public int idcalendario          { get; set; }
        public int ResultadoLocal        { get; set; }
        public int ResultadoVisitante    { get; set; }
        public int idusuario             { get; set; }
    }
}