﻿using Nancy;
using Nancy.Security;
using Mundial.Services;

namespace Mundial.Modules
{
    public class CreateUserModule : NancyModule
    {
        public CreateUserModule() : base("CreateUser")
        {
            this.RequiresAuthentication();

            //Get["/{nombre}/{usaurio}/{contrasena}/{idperfil}"] = parameters =>
            Get["/{nombre}"] = parameters =>
            {
                var nombre = parameters.nombre;
                var usuario = Request.Query.usuario;
                var contraseña = Request.Query.contrasena;
                var idPerfil = Request.Query.idperfil;

                var status = CreateUserService.GetStatus(nombre, usuario, contraseña, idPerfil);                

                return status;
            };
        }
    }
}