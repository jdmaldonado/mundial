﻿using Nancy;
using Mundial.Services;

namespace Mundial.Modules
{
    public class ProfileModule : NancyModule
    {
        public ProfileModule() : base("profiles")
        {
            Get["/"] = parameters =>
            {
                var perfiles = ProfileService.GetProfiles();

                return perfiles;
            };
        }
    }
}