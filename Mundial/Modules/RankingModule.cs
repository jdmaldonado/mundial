﻿using Nancy;
using Mundial.Services;

namespace Mundial.Modules
{
    public class RankingModule : NancyModule
    {
        public RankingModule() : base("ranking")
        {
            Get["/"] = parameters =>
            {
                var ranking = RankingService.GetRanking();

                return ranking;
            };
        }
    }
}