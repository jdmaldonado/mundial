﻿using Nancy;
using Mundial.Services;

namespace Mundial.Modules
{
    public class UserModule : NancyModule
    {
        public UserModule() : base("user")
        {
            Get["/"] = parameters =>
            {
                var user = UserService.GetUserInfo(this.Context.CurrentUser.UserName);

                return user;
            };
        }
    }
}