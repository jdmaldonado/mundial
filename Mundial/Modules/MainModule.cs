﻿using Nancy;

namespace Mundial.Modules
{
    public class MainModule : NancyModule
    {
        public MainModule()
        {
            Get["/"] = parameters => View["index"]; 
        }
    }
}