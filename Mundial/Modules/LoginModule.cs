﻿using Nancy;
using Nancy.Security;

namespace Mundial.Modules
{
    public class LoginModule : NancyModule
    {
        public LoginModule(): base("/Login")
        {
            this.RequiresAuthentication();
            Get["/"] = parameters =>
            {
                return new Response { StatusCode = HttpStatusCode.OK };
            };
        }
    }
}