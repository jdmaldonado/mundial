﻿using Nancy;
using Nancy.Security;
using Mundial.Services;

namespace Mundial.Modules
{
    public class InsertScoreModule : NancyModule
    {
        public InsertScoreModule() : base("InsertScore")
        {
            this.RequiresAuthentication();

            Get["/{idcalendario}"] = parameters =>
            {
                var idcalendario = parameters.idcalendario;
                var local = Request.Query.local;
                var visitante = Request.Query.visitante;
                var usuario = this.Context.CurrentUser.UserName;

                var status = InsertScoreService.GetStatus(idcalendario, local, visitante, usuario);
                //.GetStatus(nombre, usuario, contraseña, idPerfil);                

                return status;
            };
        }
    }
}