﻿using Nancy;
using Mundial.Services;

namespace Mundial.Modules
{
    public class TeamsModule : NancyModule
    {
        public TeamsModule() : base("teams")
        {
            Get["/"] = parameters =>
            {
                var equipos = TeamsService.GetTeams();

                return equipos;
            };
        }
    }
}