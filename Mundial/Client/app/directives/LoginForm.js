﻿MundialApp.directive('loginForm', function () {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            elem.removeClass('waiting-for-angular');

            var login = elem.find('#login-holder');
            var main = elem.find('#content');

            main.hide();

            scope.$on('event:auth-loginRequired', function () {
                login.slideDown('slow', function () {
                    main.hide();
                });
            });
            scope.$on('event:auth-loginConfirmed', function () {
                main.show();
                login.slideUp();
            });
        }
    }
});