MundialApp.controller('loginCtrl', function ($scope, $rootScope, $http, authService, httpAuth, userInfo, userInfoHome)
{
    var destroyed = false;
    if (destroyed)
        return;



    $scope.info = [];

    $scope.credentials = {
        username: "",
        password: ""
    };


    $scope.submit = function ()
    {
        httpAuth.basic($scope.credentials);
        $http.get('Login')

            .success(function ()
            {
                $scope.credentials.username = '';
                $scope.credentials.password = '';
                authService.loginConfirmed();           
                

            })
            .error(function () {
                alert("Usuario y/o Contraseņa Incorrectos");
            });
            
        userInfo.query(function (data) {
            $scope.info = data;
            //alert($scope.info.nombre);
            userInfoHome.setInfo($scope.info);
        });      

    };

    $scope.$on('$destroy', function () {
        destroyed = true;
    });
 
});

