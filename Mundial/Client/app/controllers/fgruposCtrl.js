MundialApp.controller('fgruposCtrl', function fgruposCtrl($scope, $rootScope, teamsInfo, InsertScore) {


    $scope.mensaje = "Fase De Grupos";
    $scope.paises = [];
    $scope.grupoA = [];
    $scope.grupoB = [];
    $scope.grupoC = [];
    $scope.grupoD = [];
    $scope.grupoE = [];
    $scope.grupoF = [];
    $scope.grupoG = [];
    $scope.grupoH = [];
    $scope.marcador = {};
    $scope.status = false;


    teamsInfo.query(function (data) {
        evaluar(data);
    });

    $scope.IngresarMarcador = function (obj) {
        $scope.ejemplo = [];
        $scope.ejemplo.push(obj);
    };

    var evaluar = function (data) {
        for (var i = 0 ; i < data.length ; i++) {

            /*Corrige el formato de las fechas traidas desde el servidor*/
            data[i].fecha = eval("new " + data[i].fecha.replace(/[\\/]/g, ""))

            

            /*ORGANIZATE BY GROUPS*/
            if (data[i].grupo == "Grupo A") {
                $scope.grupoA.push(data[i]);
            }
            if (data[i].grupo == "Grupo B") {
                $scope.grupoB.push(data[i]);
            }
            if (data[i].grupo == "Grupo C") {
                $scope.grupoC.push(data[i]);
            }
            if (data[i].grupo == "Grupo D") {
                $scope.grupoD.push(data[i]);
            }
            if (data[i].grupo == "Grupo E") {
                $scope.grupoE.push(data[i]);
            }
            if (data[i].grupo == "Grupo F") {
                $scope.grupoF.push(data[i]);
            }
            if (data[i].grupo == "Grupo G") {
                $scope.grupoG.push(data[i]);
            }
            if (data[i].grupo == "Grupo H") {
                $scope.grupoH.push(data[i]);
            }
        }
    }

    $scope.IngresarResultado = function (obj, l, v) {

        $scope.status = false;
        InsertScore.InsertarMarcador(obj.id, l, v, function (data) {
            $scope.status = data;
        });
    }


});
