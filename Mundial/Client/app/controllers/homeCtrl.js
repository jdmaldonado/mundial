MundialApp.controller('homeCtrl', function homeCtrl($scope, $rootScope, $timeout, userInfoHome) {

    var destroyed = false;
    if (destroyed)
        return;

    /*$rootScope.$on('UserInfo', function (event, data) {     
        $scope.nombre = data.nombre;
    });*/


    $scope.linkAnchors = function () {
        $('ul.nav a').click(function () {
            var path = $(this).attr('href');
            if (path != '#') {
                window.location = path;
            }
        });
    };
    $scope.nombre = userInfoHome.getInfo();


    $scope.$on('$destroy', function () {
        destroyed = true;
    });

    function startTimer(sectionId) {
        document.getElementById(sectionId).getElementsByTagName('timer')[0].start();
    }

    function stopTimer(sectionId) {
        document.getElementById(sectionId).getElementsByTagName('timer')[0].stop();
    }


    function addCDSeconds(sectionId, extraTime) {
        document.getElementById(sectionId).getElementsByTagName('timer')[0].addCDSeconds(extraTime);
    }

    function stopResumeTimer(sectionId, btn) {
        if (btn.innerHTML === 'Start') {
            document.getElementById(sectionId).getElementsByTagName('timer')[0].start();
            btn.innerHTML = 'Stop';
        }
        else if (btn.innerHTML === 'Stop') {
            document.getElementById(sectionId).getElementsByTagName('timer')[0].stop();
            btn.innerHTML = 'Resume';
        }
        else {
            document.getElementById(sectionId).getElementsByTagName('timer')[0].resume();
            btn.innerHTML = 'Stop';
        }
    }

});

