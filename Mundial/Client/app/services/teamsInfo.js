﻿MundialApp.factory("teamsInfo", function ($resource) {
  return $resource(
         "teams/:id",
         {
           id: "@id"
         },
         {
           "update": { method: "PUT" }
         }
    );
})