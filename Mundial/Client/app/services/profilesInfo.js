﻿MundialApp.factory("profilesInfo", function ($resource) {
  return $resource(
         "profiles/:id",
         {
           id: "@id"
         },
         {
           "update": { method: "PUT" }
         }
    );
})