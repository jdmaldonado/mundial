﻿MundialApp.factory("userInfoHome", function ($resource) {
    var destroyed = false;
    if (destroyed)
        return;


    var userQuery = {};

    return {
        getInfo: function () {
            return userQuery;
        },

        setInfo: function (data) {
            userQuery = data;
        }
    };


    $scope.$on('$destroy', function () {
        destroyed = true;
    });
});