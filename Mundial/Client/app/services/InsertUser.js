﻿MundialApp.factory("InsertUser", function ($resource) {
  var status = $resource(
         //"CreateUser/:nombre/:usuario/:contrasena:/:idperfil",
         "CreateUser/:nombre",
         {
             nombre: "@nombre"/*,
             usuario: "@usuario",
             contrasena: "@contrasena",
             idperfil: "@idperfil"*/
         },
         {
           "update": { method: "PUT" }
         }
    );

  return {
    CrearUsuario: function (nombre, usuario, contraseña, idperfil, callback) {
      status.query({ nombre: nombre, usuario: usuario, contrasena: contrasena, idperfil: idperfil}, function (data) {
        callback(data);
      });
    }
  };

})