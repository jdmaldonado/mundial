﻿MundialApp.factory("InsertScore", function ($resource) {
  var status = $resource(
         "InsertScore/:idcalendario",
         {
             idcalendario: "@idcalendario",
         },
         {
           "update": { method: "PUT" }
         }
    );

  return {
    InsertarMarcador: function (idcalendario, local, visitante, callback) {
      status.query({ idcalendario: idcalendario, local: local, visitante: visitante }, function (data) {
        callback(data);
      });
    }
  };

})