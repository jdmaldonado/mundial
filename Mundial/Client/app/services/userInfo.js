﻿MundialApp.factory("userInfo", function ($resource) {
  return   $resource(
         "user/:id",
         {
           id: "@id"
         },
         {
           "update": { method: "PUT" }
         }
    );  
})