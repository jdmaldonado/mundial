﻿MundialApp.factory("getRanking", function ($resource) {
  return   $resource(
         "ranking/:id",
         {
           id: "@id"
         },
         {
           "update": { method: "PUT" }
         }
    );  
})