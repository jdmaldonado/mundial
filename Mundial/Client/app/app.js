﻿var MundialApp = angular.module("MundialApp", ['ngResource', 'ui', 'http-auth-interceptor', 'ur.http.auth','timer'])

  .config(function ($routeProvider, $httpProvider) {

      $routeProvider

        .when('/login', {
            controller: 'loginCtrl',
            templateUrl: 'Client/app/templates/loginTemplate.html'
        })

        .when('/', {
            controller: 'homeCtrl',
            templateUrl: 'Client/app/templates/homeTemplate.html'
        })

        .when('/fgrupos', {
            controller: 'fgruposCtrl',
            templateUrl: 'Client/app/templates/fgruposTemplate.html'
        })

        .when('/ranking', {
            controller: 'rankingCtrl',
            templateUrl: 'Client/app/templates/rankingTemplate.html'
        })

        .when('/createUser', {
            controller: 'createUserCtrl',
            templateUrl: 'Client/app/templates/createUserTemplate.html'
        })

        .otherwise({ redirectTo: '/' });

  });
