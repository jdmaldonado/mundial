﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Mundial.Models;

namespace Mundial.DataBase.Inserts
{
    public class InsertScoreInsert
    {
        public static dynamic ExecuteInsert(string idCalendario, string local, string visitante, string idUsuario, int tipo, int Id )
        {
            var db = Database.OpenNamedConnection("LOCAL");

            if (tipo == 1)
            {

                Score scoreInfo = new Score();

                scoreInfo.idcalendario = Convert.ToInt32(idCalendario);
                scoreInfo.ResultadoLocal = Convert.ToInt32(local);
                scoreInfo.ResultadoVisitante = Convert.ToInt32(visitante);
                scoreInfo.idusuario = Convert.ToInt32(idUsuario);


                var status = db.Resultados.Insert(scoreInfo);

                return status;

            }
            else
            {
                dynamic record = new SimpleRecord();
                record.IdResultado          = Id;
                record.ResultadoLocal       = local;
                record.ResultadoVisitante   = visitante;

                var status = db.Resultados.UpdateByIdResultado(record);

                return status;
            }        


            
        }
    }
}