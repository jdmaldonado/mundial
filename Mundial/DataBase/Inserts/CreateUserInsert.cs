﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Mundial.Models;

namespace Mundial.DataBase.Inserts
{
    public class CreateUserInsert
    {
        public static dynamic ExecuteInsert(string nombre, string usuario, string contraseña, int idPerfil)
        {
            var db = Database.OpenNamedConnection("LOCAL");

            UserInfo userInfo = new UserInfo();

            userInfo.nombre = nombre;
            userInfo.usuario = usuario;
            userInfo.contraseña = contraseña;
            userInfo.idPerfil = idPerfil;


            var status = db.Usuarios.Insert(userInfo);


            return status;
        }
    }
}