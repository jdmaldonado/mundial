﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;

namespace Mundial.DataBase.Queries
{
    public class GetTeamsQuery
    {
        public static IEnumerable<T> ExecuteGiven<T>()
        {
            var db = Database.OpenNamedConnection("LOCAL");
            var teams = db.Calendario.All().ToList<T>();


            return teams;
        }
    }
}