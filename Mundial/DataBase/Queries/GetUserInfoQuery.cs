﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;

namespace Mundial.DataBase.Queries
{
    public class GetUserInfoQuery
    {
        public static dynamic ExecuteGiven(string username)
        {
            var db = Database.OpenNamedConnection("LOCAL");
            var userInfo = db.Usuarios.Find(db.Usuarios.Usuario == username );

            return userInfo;
        }
    }
}