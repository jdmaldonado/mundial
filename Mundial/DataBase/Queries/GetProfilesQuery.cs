﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;

namespace Mundial.DataBase.Queries
{
    public class GetProfilesQuery
    {
        public static IEnumerable<T> ExecuteGiven<T>()
        {
            var db = Database.OpenNamedConnection("LOCAL");
            var profiles = db.Perfiles.All().ToList<T>();


            return profiles;
        }
    }
}