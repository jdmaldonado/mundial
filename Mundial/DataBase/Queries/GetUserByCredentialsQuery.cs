﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;

namespace Mundial.DataBase.Queries
{
    public class GetUserByCredentialsQuery
    {
        public static dynamic ExecuteGiven(string username, string password)
        {
            var db = Database.OpenNamedConnection("LOCAL");
            var user = db.Usuarios.Find(db.Usuarios.Usuario == username && db.Usuarios.Contraseña == password);

            return user;
        }
    }
}