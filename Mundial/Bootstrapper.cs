﻿using System;
using Nancy;
using Nancy.Authentication.Basic;
using Nancy.Bootstrapper;
using Nancy.Conventions;

namespace Mundial
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);

            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("Client", "Client"));
        }

        protected override void ApplicationStartup(Nancy.TinyIoc.TinyIoCContainer container, Nancy.Bootstrapper.IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            ServiceStack.Text.JsConfig.EmitCamelCaseNames = true;
            ServiceStack.Text.JsConfig.TreatEnumAsInteger = true;

            pipelines.EnableBasicAuthentication(new BasicAuthenticationConfiguration(
            container.Resolve<IUserValidator>(),
            "MyRealm"));
        }

        protected override void RequestStartup(Nancy.TinyIoc.TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            pipelines.OnError.AddItemToEndOfPipeline((ctx, exception) =>
            {
                Log.Instance.ErrorException(exception.Message, exception);

                if (exception is ArgumentException)
                {
                    return new Response { StatusCode = HttpStatusCode.BadRequest }
                        .WithHeader("ReasonPhrase", exception.Message); ;
                }

                return new Response { StatusCode = HttpStatusCode.InternalServerError }
                    .WithHeader("ReasonPhrase", exception.Message); ;
            });

            base.RequestStartup(container, pipelines, context);
        }
    }
}